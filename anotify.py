# -*- coding: utf-8 -*-
#
# anotify.py
# Copyright (c) 2012 magnific0
#
# based on:
# growl.py
# Copyright (c) 2011 Sorin Ionescu <sorin.ionescu@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


SCRIPT_NAME = 'anotify'
SCRIPT_AUTHOR = 'magnific0'
SCRIPT_VERSION = '1.0.0'
SCRIPT_LICENSE = 'MIT'
SCRIPT_DESC = 'Sends libnotify notifications upon events.'


# Changelog
# 2012-09-20: v1.0.0 Forked from original and adapted for libnotify.

# -----------------------------------------------------------------------------
# Settings
# -----------------------------------------------------------------------------
SETTINGS = {
    'show_public_message': 'off',
    'show_private_message': 'on',
    'show_public_action_message': 'off',
    'show_private_action_message': 'on',
    'show_notice_message': 'off',
    'show_invite_message': 'on',
    'show_highlighted_message': 'on',
    'show_server': 'on',
    'show_channel_topic': 'on',
    'show_dcc': 'on',
    'show_upgrade_ended': 'on',
    'sticky': 'off',
    'sticky_away': 'on',
    'icon': '/usr/share/pixmaps/weechat.xpm',
    'timeout': '10',
    'merge': 'on',
}


# -----------------------------------------------------------------------------
# Imports
# -----------------------------------------------------------------------------
import bisect
import re
import os
import time


try:
    import weechat
except ImportError as error:
    print('This script must be run under WeeChat.')
    print('Get WeeChat at http://www.weechat.org.')
    raise error
try:
    import gobject
    import pynotify
except ImportError as error:
    weechat.prnt('', 'anotify: {0}'.format(error))
    raise error

# -----------------------------------------------------------------------------
# Globals
# -----------------------------------------------------------------------------
TAGGED_MESSAGES = {
    'public message or action': set(['irc_privmsg', 'notify_message']),
    'private message or action': set(['irc_privmsg', 'notify_private']),
    'notice message': set(['irc_notice', 'notify_private']),
    'invite message': set(['irc_invite', 'notify_highlight']),
    'channel topic': set(['irc_topic', ]),
    # 'away status': set(['away_info', ]),
}


UNTAGGED_MESSAGES = {
    'away status':
        re.compile(r'^You ((\w+).){2,3}marked as being away', re.UNICODE),
    'dcc chat request':
        re.compile(r'^xfer: incoming chat request from (\w+)', re.UNICODE),
    'dcc chat closed':
        re.compile(r'^xfer: chat closed with (\w+)', re.UNICODE),
    'dcc get request':
        re.compile(
            r'^xfer: incoming file from (\w+) [^:]+: ((?:,\w|[^,])+),',
            re.UNICODE),
    'dcc get completed':
        re.compile(r'^xfer: file ([^\s]+) received from \w+: OK', re.UNICODE),
    'dcc get failed':
        re.compile(
            r'^xfer: file ([^\s]+) received from \w+: FAILED',
            re.UNICODE),
    'dcc send completed':
        re.compile(r'^xfer: file ([^\s]+) sent to \w+: OK', re.UNICODE),
    'dcc send failed':
        re.compile(r'^xfer: file ([^\s]+) sent to \w+: FAILED', re.UNICODE),
}


DISPATCH_TABLE = {
    'away status': 'set_away_status',
    'public message or action': 'notify_public_message_or_action',
    'private message or action': 'notify_private_message_or_action',
    'notice message': 'notify_notice_message',
    'invite message': 'notify_invite_message',
    'channel topic': 'notify_channel_topic',
    'dcc chat request': 'notify_dcc_chat_request',
    'dcc chat closed': 'notify_dcc_chat_closed',
    'dcc get request': 'notify_dcc_get_request',
    'dcc get completed': 'notify_dcc_get_completed',
    'dcc get failed': 'notify_dcc_get_failed',
    'dcc send completed': 'notify_dcc_send_completed',
    'dcc send failed': 'notify_dcc_send_failed',
}


STATE = {
    'icon': None,
    'is_away': False
}


# -----------------------------------------------------------------------------
# Notifiers
# -----------------------------------------------------------------------------
def cb_irc_server_connected(data, signal, signal_data):
    '''Notify when connected to IRC server.'''
    if weechat.config_get_plugin('show_server') == 'on':
        a_notify(
            'server_connected',
            'Server',
            'Server Connected',
            signal_data)
    return weechat.WEECHAT_RC_OK


def cb_irc_server_disconnected(data, signal, signal_data):
    '''Notify when disconnected to IRC server.'''
    if weechat.config_get_plugin('show_server') == 'on':
        a_notify(
            'server_disconnected',
            'Server',
            'Server Disconnected',
            'Disconnected from network {0}.'.format(signal_data))
    return weechat.WEECHAT_RC_OK


def cb_notify_upgrade_ended(data, signal, signal_data):
    '''Notify on end of WeeChat upgrade.'''
    if weechat.config_get_plugin('show_upgrade_ended') == 'on':
        a_notify(
            'client_upgraded',
            'WeeChat',
            'WeeChat Upgraded',
            'WeeChat has been upgraded.')
    return weechat.WEECHAT_RC_OK


def notify_highlighted_message(bufname, prefix, message):
    '''Notify on highlighted message.'''
    if weechat.config_get_plugin("show_highlighted_message") == "on":
        a_notify(
            bufname,
            'Highlight',
            'Highlighted Message',
            "{0}: {1}".format(prefix, message),
            priority=pynotify.URGENCY_CRITICAL)


def notify_public_message_or_action(bufname, prefix, message, highlighted):
    '''Notify on public message or action.'''
    if prefix == ' *':
        regex = re.compile(r'^(\w+) (.+)$', re.UNICODE)
        match = regex.match(message)
        if match:
            prefix = match.group(1)
            message = match.group(2)
            notify_public_action_message(prefix, message, highlighted)
    else:
        if highlighted:
            notify_highlighted_message(prefix, message)
        elif weechat.config_get_plugin("show_public_message") == "on":
            a_notify(
                bufname,
                'Public',
                'Public Message',
                '{0}: {1}'.format(prefix, message))


def notify_private_message_or_action(bufname, prefix, message, highlighted):
    '''Notify on private message or action.'''
    regex = re.compile(r'^CTCP_MESSAGE.+?ACTION (.+)$', re.UNICODE)
    match = regex.match(message)
    if match:
        notify_private_action_message(bufname, prefix, match.group(1), highlighted)
    else:
        if prefix == ' *':
            regex = re.compile(r'^(\w+) (.+)$', re.UNICODE)
            match = regex.match(message)
            if match:
                prefix = match.group(1)
                message = match.group(2)
                notify_private_action_message(bufname, prefix, message, highlighted)
        else:
            if highlighted:
                notify_highlighted_message(prefix, message)
            elif weechat.config_get_plugin("show_private_message") == "on":
                a_notify(
                    bufname,
                    'Private',
                    'Private Message',
                    '{0}: {1}'.format(prefix, message))


def notify_public_action_message(bufname, prefix, message, highlighted):
    '''Notify on public action message.'''
    if highlighted:
        notify_highlighted_message(prefix, message)
    elif weechat.config_get_plugin("show_public_action_message") == "on":
        a_notify(
            bufname,
            'Action',
            'Public Action Message',
            '{0}: {1}'.format(prefix, message),
            priority=pynotify.URGENCY_NORMAL)


def notify_private_action_message(bufname, prefix, message, highlighted):
    '''Notify on private action message.'''
    if highlighted:
        notify_highlighted_message(prefix, message)
    elif weechat.config_get_plugin("show_private_action_message") == "on":
        a_notify(
            bufname,
            'Action',
            'Private Action Message',
            '{0}: {1}'.format(prefix, message),
            priority=pynotify.URGENCY_NORMAL)


def notify_notice_message(bufname, prefix, message, highlighted):
    '''Notify on notice message.'''
    regex = re.compile(r'^([^\s]*) [^:]*: (.+)$', re.UNICODE)
    match = regex.match(message)
    if match:
        prefix = match.group(1)
        message = match.group(2)
        if highlighted:
            notify_highlighted_message(prefix, message)
        elif weechat.config_get_plugin("show_notice_message") == "on":
            a_notify(
                bufname,
                'Notice',
                'Notice Message',
                '{0}: {1}'.format(prefix, message))


def notify_invite_message(bufname, prefix, message, highlighted):
    '''Notify on channel invitation message.'''
    if weechat.config_get_plugin("show_invite_message") == "on":
        regex = re.compile(
            r'^You have been invited to ([^\s]+) by ([^\s]+)$', re.UNICODE)
        match = regex.match(message)
        if match:
            channel = match.group(1)
            nick = match.group(2)
            a_notify(
                bufname,
                'Invite',
                'Channel Invitation',
                '{0} has invited you to join {1}.'.format(nick, channel))


def notify_channel_topic(bufname, prefix, message, highlighted):
    '''Notify on channel topic change.'''
    if weechat.config_get_plugin("show_channel_topic") == "on":
        regex = re.compile(
            r'^\w+ has (?:changed|unset) topic for ([^\s]+)' +
            '(?:(?: from "(?:(?:"\w|[^"])+)")? to "((?:"\w|[^"])+)")?',
            re.UNICODE)
        match = regex.match(message)
        if match:
            channel = match.group(1)
            topic = match.group(2) or ''
            a_notify(
                bufname,
                'Channel',
                'Channel Topic',
                "{0}: {1}".format(channel, topic))


def notify_dcc_chat_request(bufname, match):
    '''Notify on DCC chat request.'''
    if weechat.config_get_plugin("show_dcc") == "on":
        nick = match.group(1)
        a_notify(
            bufname,
            'DCC',
            'Direct Chat Request',
            '{0} wants to chat directly.'.format(nick))


def notify_dcc_chat_closed(bufname, match):
    '''Notify on DCC chat termination.'''
    if weechat.config_get_plugin("show_dcc") == "on":
        nick = match.group(1)
        a_notify(
            bufname,
            'DCC',
            'Direct Chat Ended',
            'Direct chat with {0} has ended.'.format(nick))


def notify_dcc_get_request(bufname, match):
    'Notify on DCC get request.'
    if weechat.config_get_plugin("show_dcc") == "on":
        nick = match.group(1)
        file_name = match.group(2)
        a_notify(
            bufname,
            'DCC',
            'File Transfer Request',
            '{0} wants to send you {1}.'.format(nick, file_name))


def notify_dcc_get_completed(bufname, match):
    'Notify on DCC get completion.'
    if weechat.config_get_plugin("show_dcc") == "on":
        file_name = match.group(1)
        a_notify(bufname, 'DCC', 'Download Complete', file_name)


def notify_dcc_get_failed(bufname, match):
    'Notify on DCC get failure.'
    if weechat.config_get_plugin("show_dcc") == "on":
        file_name = match.group(1)
        a_notify(bufname, 'DCC', 'Download Failed', file_name)


def notify_dcc_send_completed(bufname, match):
    'Notify on DCC send completion.'
    if weechat.config_get_plugin("show_dcc") == "on":
        file_name = match.group(1)
        a_notify(bufname, 'DCC', 'Upload Complete', file_name)


def notify_dcc_send_failed(bufname, match):
    'Notify on DCC send failure.'
    if weechat.config_get_plugin("show_dcc") == "on":
        file_name = match.group(1)
        a_notify(bufname, 'DCC', 'Upload Failed', file_name)


# -----------------------------------------------------------------------------
# Utility
# -----------------------------------------------------------------------------
def set_away_status(bufname, match):
    status = match.group(1)
    if status == 'been ':
        STATE['is_away'] = True
    if status == 'longer ':
        STATE['is_away'] = False


def cb_process_message(
    data,
    wbuffer,
    date,
    tags,
    displayed,
    highlight,
    prefix,
    message
):
    '''Delegates incoming messages to appropriate handlers.'''
    tags = set(tags.split(','))
    functions = globals()
    is_public_message = tags.issuperset(
        TAGGED_MESSAGES['public message or action'])
    buffer_name = weechat.buffer_get_string(wbuffer, 'name')
    dcc_buffer_regex = re.compile(r'^irc_dcc\.', re.UNICODE)
    dcc_buffer_match = dcc_buffer_regex.match(buffer_name)
    highlighted = False
    if highlight == "1":
        highlighted = True
    # Private DCC message identifies itself as public.
    if is_public_message and dcc_buffer_match:
        notify_private_message_or_action(
            buffer_name, prefix, message, highlighted)
        return weechat.WEECHAT_RC_OK
    # Pass identified, untagged message to its designated function.
    for key, value in UNTAGGED_MESSAGES.items():
        match = value.match(message)
        if match:
            functions[DISPATCH_TABLE[key]](buffer_name, match)
            return weechat.WEECHAT_RC_OK
    # Pass identified, tagged message to its designated function.
    for key, value in TAGGED_MESSAGES.items():
        if tags.issuperset(value):
            functions[DISPATCH_TABLE[key]](
                buffer_name, prefix, message, highlighted)
            return weechat.WEECHAT_RC_OK

    if 'nick_%s' % weechat.buffer_get_string(wbuffer, 'localvar_nick') in tags:
        notify_user_message(buffer_name)
    return weechat.WEECHAT_RC_OK

def notify_user_message(bufname):
    '''Clear buffer notification state when user sends message.'''
    try:
        BUFFERS.pop(bufname)
    except KeyError:
        pass




BUFFERS = {}


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


class BufferState(object):
    def __init__(
            self,
            notification_title,
            hysteresis_threshold,
            hysteresis_refresh,
            icon="/usr/share/pixmaps/weechat.xpm",
            priority=pynotify.URGENCY_LOW,
            timeout=1,
            ):

        self._title, self._icon = notification_title, icon
        self._timeout = timeout
        self._priority = priority
        self._msgs = []
        self._started = False
        self.note = None

        # Hysteresis
        self._accumulated = 0.0
        self._window = hysteresis_threshold / 2
        self._threshold = hysteresis_threshold
        self._refresh = hysteresis_refresh

    def _clear_cb(self, note):
        # weechat.prnt('', 'anotify: clearing {0}'.format(note))
        self.note = None
        self._msgs = []

    def _show(self):
        desc = '\n'.join(m for _, m in self._msgs)

        if self.note:
            self.note.update(self._title, desc, self._icon)

        else:
            self.note = pynotify.Notification(self._title, desc, self._icon)
            # weechat.prnt('', 'anotify: new notifier {0}'.format(self.note))
            self.note.connect('closed', self._clear_cb)
            self.note.set_urgency(self._priority)
            self.note.set_timeout(self._timeout)

        self.note.show()

    def append(self, msg, at_time=None, merge=True):
        at_time = at_time or time.time()

        if merge:
            self._msgs.append(((at_time + self._timeout), msg))
            del self._msgs[:-10]
        else:
            self._msgs = [(float("inf"), msg)]

        if not self._started:
            return

        if self.note:
            self._show()

        else:
            # Hysteresis
            self._accumulated += self._window
            self._window = max(self._window / 2.0, self._refresh)

            if (self._accumulated > self._threshold
                    or isclose(self._accumulated, self._threshold)):
                self._show()

                self._accumulated = 0.0

    def start(self):
        self._started = True
        self._show()

    def tick(self):
        self._window += self._refresh
        del self._msgs[:bisect.bisect_left(self._msgs, (time.time(), ''))]
        return (self._window < self._threshold
                and not isclose(self._window, self._threshold))


def cb_buffer_start(bname, remaining_calls):
    try:
        buf = BUFFERS[bname]
        buf.start()
    except:
        return weechat.WEECHAT_RC_ERROR

    return weechat.WEECHAT_RC_OK


def cb_buffer_tick(arg, remaining_calls):
    for buf in list(BUFFERS.keys()):
        if not BUFFERS[buf].tick():
            # weechat.prnt('', 'anotify: removing notifier {0}'.format(BUFFERS[buf]._window))
            BUFFERS.pop(buf)

    if remaining_calls == 0:
        weechat.hook_timer(1000, 1, 65535, "cb_buffer_tick", arg)

    return weechat.WEECHAT_RC_OK


def a_notify(bname, nclass, title, description, priority=pynotify.URGENCY_LOW):
    '''Creates or updates the notification'''
    is_away = STATE['is_away']
    icon = STATE['icon']
    time_out = int(weechat.config_get_plugin("timeout"))
    cur_time = time.time()
    threshold = 1.0
    refresh = 0.01

    try:
        BUFFERS[bname].append(
                description,
                merge=weechat.config_get_plugin("merge") == "on",
                )
    except KeyError:
        BUFFERS[bname] = BufferState(
                title,
                threshold,
                refresh,
                icon=icon,
                priority=priority,
                timeout=time_out,
                )
        BUFFERS[bname].append(
                description,
                merge=weechat.config_get_plugin("merge") == "on",
                )
        weechat.hook_timer(500, 0, 1, "cb_buffer_start", bname)


# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------
def main():
    '''Sets up WeeChat notifications.'''
    # Initialize options.
    for option, value in SETTINGS.items():
        if not weechat.config_is_set_plugin(option):
            weechat.config_set_plugin(option, value)
    # Initialize.
    name = "WeeChat"
    icon = "/usr/share/pixmaps/weechat.xpm"
    notifications = [
        'Public',
        'Private',
        'Action',
        'Notice',
        'Invite',
        'Highlight',
        'Server',
        'Channel',
        'DCC',
        'WeeChat'
    ]
    STATE['icon'] = icon
    # Register hooks.
    weechat.hook_signal(
        'irc_server_connected',
        'cb_irc_server_connected',
        '')
    weechat.hook_signal(
        'irc_server_disconnected',
        'cb_irc_server_disconnected',
        '')
    weechat.hook_signal('upgrade_ended', 'cb_upgrade_ended', '')
    weechat.hook_print('', '', '', 1, 'cb_process_message', '')
    weechat.hook_timer(1000, 1, 65535, "cb_buffer_tick", "")
    pynotify.init(name)


if __name__ == '__main__' and weechat.register(
    SCRIPT_NAME,
    SCRIPT_AUTHOR,
    SCRIPT_VERSION,
    SCRIPT_LICENSE,
    SCRIPT_DESC,
    '',
    ''
):
    main()
